<?php


namespace Dunco\Services;

use Dunco\Models\CustomerLegalEntity;
use Dunco\Models\DriverPayout;
use Dunco\Models\Invoice;
use Dunco\Models\PaymentProvider;

class CountCleRate
{
    /**
     * @var
     */
    public $cle_id;
    /**
     * @var CustomerLegalEntity
     */
    protected $cle;

    /**
     * CountCleRate constructor.
     * @param CustomerLegalEntity $cle
     */
    public function __construct(CustomerLegalEntity $cle)
    {
        $this->cle = $cle;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function count()
    {


        $start = new \DateTime("first day of last month");
        $end = new \DateTime("last day of last month");


        $providers = PaymentProvider::where('customer_legal_entity_id', $this->cle->id)
            ->distinct('type')
            ->get()
            ->pluck('type');

        foreach ($providers as $provider) {
            $driver_payouts_sum_amount = DriverPayout::where('status', 'success')
                ->whereBetween('created_at', [$start->format('Y-m-d') . " 00:00:00", $end->format('Y-m-d') . " 23:59:59"])
                ->whereHas('paymentProvider', function ($query) use ($provider) {
                    $query->where('type', $provider);
                })
                ->whereHas('driver', function ($query) {
                    $query->whereHas('park', function ($query) {
                        $query->whereHas('customerLegalEntity', function ($query) {
                            $query->where('id', $this->cle->id);
                        });
                    });
                })->sum('amount');

            $rate = $this->cle->customRates($provider)
                ->where('sum', '>=', $driver_payouts_sum_amount)
                ->orderBy('sum', 'asc')
                ->first();

            // todo: костыль, если верхняя граница не попадет в наши рейты. чтобы взял комиссию по самому большому обороту в любом случае
            if (is_null($rate)) {
                $rate = $this->cle->customRates($provider)->orderBy('sum', 'desc')->first();
            }

            // рассчитаем для процентов
            if (is_null($rate->fix) && !is_null($rate->percent) && is_null($rate->per_txn)) {
                $sum = round($driver_payouts_sum_amount * $rate->percent / 100, 2);
            }

            // рассчитаем для фикса за транзакцию
            if (is_null($rate->fix) && is_null($rate->percent) && !is_null($rate->per_txn)) {
                $sum = DriverPayout::where('status', 'success')
                        ->whereBetween('created_at', [$start->format('Y-m-d') . " 00:00:00", $end->format('Y-m-d') . " 23:59:59"])
                        ->whereHas('paymentProvider', function ($query) use ($provider) {
                            $query->where('type', $provider);
                        })
                        ->whereHas('driver', function ($query) {
                            $query->whereHas('park', function ($query) {
                                $query->whereHas('customerLegalEntity', function ($query) {
                                    $query->where('id', $this->cle->id);
                                });
                            });
                        })
                        ->count('id') * $rate->per_txn;
            }
            
            // поставим фикс
            if (!is_null($rate->fix) && is_null($rate->percent) && is_null($rate->per_txn)) {
                $sum = $rate->fix;
            }
dump($sum);
            if ($sum > 0) {
                Invoice::create([
                    'sum' => $driver_payouts_sum_amount,
                    'commission' => $sum,
                    'customer_legal_entity_id' => $this->cle->id,
                    'rate' => $rate->rate,
                    'date' => $end,
                    'payment_provider_type' => $provider
                ]);
            }

        }


    }
}
